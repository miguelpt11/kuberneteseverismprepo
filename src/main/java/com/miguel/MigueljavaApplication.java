package com.miguel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigueljavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MigueljavaApplication.class, args);
	}

}
