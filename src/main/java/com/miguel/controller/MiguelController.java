package com.miguel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/miguelpablo")
public class MiguelController {

	@GetMapping(value = "saludo", params = "nombre")
	String getSaludo(String nombre) {
		return "Hola coder: " + nombre;
	}

}
